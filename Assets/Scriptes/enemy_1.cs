﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy_1 : MonoBehaviour {

    public int hp = 100;
    private float speed = 1.5f;
    private float jumpforce = 5.0f;
    public bool FaceRight = false;
    new private Rigidbody2D rigidbody;

    private bool stopped = false;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Stopping brick")
            stopped = true;
        if (other.gameObject.tag == "Bullet")
            hp -= 20;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Stopping trigger")
            stopped = true;
        if (other.gameObject.tag == "Jumping trigger")
             Jump();
    }

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Jump()
    {
        rigidbody.AddForce(transform.up * jumpforce, ForceMode2D.Impulse);
    }

    private void Flip()
    {
        FaceRight = !FaceRight;
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
    }

    // Update is called once per frame
    void Update ()
    {
		if(hp <= 0)
            Destroy(gameObject);
        if (stopped == false)
        {
            transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        }
        if (stopped == true)
        {
            stopped = false;
            speed = -speed;
            Flip();
        }
	}
}
