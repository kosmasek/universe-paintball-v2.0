﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Edge : MonoBehaviour {
    public GameObject Respawn;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            other.transform.position = Respawn.transform.position;
            other.gameObject.GetComponent<Character>().hp = 100;
        }
        else
            Destroy(other.gameObject);
    }

    //private void Update()
    //{
    //    transform.position = new Vector3(Character.transform.position.x, transform.position.y, transform.position.z);
    //}
}


