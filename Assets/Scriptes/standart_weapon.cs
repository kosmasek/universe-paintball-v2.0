﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class standart_weapon : MonoBehaviour {
    private bool FaceRight = true;

    private void Flip()
    {
        FaceRight = !FaceRight;
        transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y * -1, transform.localScale.z);
    }

    void Update () {
        var mousePosition = Input.mousePosition;

        if ((mousePosition.x > Screen.width / 2f && !FaceRight) ||
    (mousePosition.x < Screen.width / 2f && FaceRight))
            Flip();

        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        var angle = Vector2.Angle(Vector2.right, mousePosition - transform.position);
        transform.eulerAngles = new Vector3(0f, 0f, transform.position.y < mousePosition.y ? angle : -angle);
    }
}
