﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableObstacle : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D coll)
    {
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        if (coll.gameObject.tag == "Player")
            coll.gameObject.GetComponent<Character>().hp -= 10;

    }
 
}
